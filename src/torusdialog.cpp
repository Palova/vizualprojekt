﻿#include "torusdialog.h"
#include "ui_torusdialog.h"

TorusDialog::TorusDialog(QDialog * parent) : TvaroDialog(parent) {
	ui = new Ui::TorusDialog();
	ui->setupUi(this); 
	this->layout()->setSizeConstraint(QLayout::SetFixedSize);
}

vtkSmartPointer<vtkActor> TorusDialog::GetActor()
{
	vtkSmartPointer<vtkParametricFunctionSource> torusSource = vtkSmartPointer<vtkParametricFunctionSource>::New();
	vtkSmartPointer<vtkParametricTorus> torus = vtkSmartPointer<vtkParametricTorus>::New();
	torus->SetRingRadius(ui->ringRadiusSpin->value());
	torus->SetCrossSectionRadius(ui->crossSectSpin->value());
	torus->JoinUOff();
	torus->JoinVOff();
	torus->JoinWOff();
	torusSource->SetParametricFunction(torus);
	torusSource->SetUResolution(ui->uResSpin->value());
	torusSource->SetVResolution(ui->vResSpin->value());
	torusSource->SetWResolution(ui->vResSpin->value());
	torusSource->Update();

	vtkSmartPointer<vtkTriangleFilter> triangulate = vtkSmartPointer<vtkTriangleFilter>::New();
	triangulate->SetInputConnection(torusSource->GetOutputPort());
	triangulate->Update();

	vtkSmartPointer<vtkPolyDataMapper> mapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputConnection(triangulate->GetOutputPort());
	actor = vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(mapper);

	return actor;
}

TorusDialog::~TorusDialog() {
	delete ui;
}

