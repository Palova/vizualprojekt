﻿#include "implicitdialog.h"

ImplicitDialog::ImplicitDialog(QDialog * parent) : TvaroDialog(parent) {
	ui = new Ui::implicitdialog();
	ui->setupUi(this);
}

vtkSmartPointer<vtkActor> ImplicitDialog::GetActor()
{
	vtkSmartPointer<vtkImageData> data = vtkSmartPointer<vtkImageData>::New();
	int xmax = 512, ymax = 512, zmax = 512;
	data->SetExtent(0, xmax - 1, 0, ymax - 1, 0, zmax - 1);

	double x_interval = ui->spinXend->value() - ui->spinXstart->value();
	double y_interval = ui->spinYend->value() - ui->spinYstart->value();
	double z_interval = ui->spinZend->value() - ui->spinZstart->value();
	double x_min = ui->spinXstart->value();
	double y_min = ui->spinYstart->value();
	double z_min = ui->spinZstart->value();
	double dx = (x_interval) / xmax;
	double dy = (y_interval) / ymax;
	double dz = (z_interval) / zmax;

//	data->SetSpacing(3.0 / xmax, 3.0 / ymax, 3.0 / zmax);
	data->SetSpacing(x_interval/xmax, y_interval/ymax, z_interval/zmax);
//	data->SetOrigin(-1.5,-1.5,-1.5);
	data->SetOrigin(x_min, y_min, z_min);
	data->AllocateScalars(VTK_FLOAT, 1);
	float *dataPtr = (float *)data->GetScalarPointer();

	std::string implLine = ui->implLine->text().toStdString();
	
	//double bod[3];
	symbol_table_t symbol_table;
	symbol_table.add_variable("x", bod[0]);
	symbol_table.add_variable("y", bod[1]);
	symbol_table.add_variable("z", bod[2]);
	symbol_table.add_constants();
	expression_t expression;
	expression.register_symbol_table(symbol_table);

	parser_t parser;
	if (!parser.compile(implLine, expression)) {
		std::cout << parser.error() << std::endl;
		return false;
	}

	//vygenerujeme 3d objem podla funkcie
	for (int i = 0; i < xmax; i++)
	{
		for (int j = 0; j < ymax; j++)
		{
			for (int k = 0; k < zmax; k++)
			{
	
				double half = xmax / 2 - 1;
				bod[0] = x_min + dx * i;
				bod[1] = y_min + dy *j;
				bod[2] = z_min + dz *k;
				dataPtr[i + xmax * (j + ymax * k)] = expression.value();
			}
		}
	}

	//vytvorime si izoplochu
	vtkSmartPointer<vtkMarchingCubes> surface = vtkSmartPointer<vtkMarchingCubes>::New();
	surface->SetInputData(data.Get());
	surface->ComputeNormalsOn();
	
	//izoplocha hodnoty 0.000001, vtk ma nejake problemy ked to je presna nula...
	surface->SetValue(0, 0.000001);
	
	vtkSmartPointer<vtkPolyDataMapper> mapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputConnection(surface->GetOutputPort());
	mapper->Update();
	
	actor = vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(mapper);
	vtkSmartPointer<vtkPolyData> polydata = vtkPolyData::SafeDownCast(mapper->GetInput());

	//odstranime farbu pridanu marching cubes
	polydata->GetPointData()->RemoveArray(0);

	//preskalujeme body na povodne hodnoty
	//double bod[3];
	size_t pocet = polydata->GetNumberOfPoints();
	for (int i = 0; i < pocet; i++)
	{
		polydata->GetPoint(i, bod);
		double x, y, z;
		double half = xmax / 2 - 1;
		x = (bod[0] - half) / (xmax)* (3.0);
		y = (bod[1] - half) / (ymax)* (3.0);
		z = (bod[2] - half) / (zmax)* (3.0);
		polydata->GetPoints()->SetPoint(i, x, y, z);
	}
	polydata->Modified();
	return actor;
}

ImplicitDialog::~ImplicitDialog() {
	delete ui;
	
}
