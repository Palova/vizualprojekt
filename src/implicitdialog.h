﻿#pragma once
#ifndef IMPLICITDIALOG_HPP
#define IMPLICITDIALOG_HPP
#include <QDialog>
#include "ui_implicitdialog.h"
#include "tvarodialog.h"

#include <vtkSmartPointer.h>
#include <vtkPlaneSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkTriangleFilter.h>
#include <vtkImageData.h>
#include <vtkMarchingCubes.h>
#include <vtkPointData.h>
#include <vtkPoints.h>

#include "exprtk.hpp"


typedef exprtk::symbol_table<double> symbol_table_t;
typedef exprtk::expression<double>     expression_t;
typedef exprtk::parser<double>             parser_t;



class ImplicitDialog : public TvaroDialog {
	Q_OBJECT

public:
	ImplicitDialog(QDialog * parent = Q_NULLPTR);
	vtkSmartPointer<vtkActor> GetActor();
	~ImplicitDialog();
	double bod[3];

private:
	Ui::implicitdialog *ui;


typedef exprtk::symbol_table<double> symbol_table_t;
typedef exprtk::expression<double>     expression_t;
typedef exprtk::parser<double>             parser_t;
};

#endif // IMPLICITDIALOG_HPP