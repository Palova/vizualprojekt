﻿#pragma once
#ifndef TORUSDIALOG_HPP
#define TORUSDIALOG_HPP
#include <QDialog>
#include "tvarodialog.h"

#include <vtkSmartPointer.h>
#include <vtkParametricFunctionSource.h>
#include <vtkSphereSource.h>
#include <vtkParametricTorus.h>
#include <vtkPlaneSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkTriangleFilter.h>
#include <vtkPolyDataWriter.h>
#include <vtkPolyDataReader.h>
#include <vtkFeatureEdges.h>
#include <vtkStripper.h>

namespace Ui {class TorusDialog;}

class TorusDialog : public TvaroDialog {
	Q_OBJECT

public:
	TorusDialog(QDialog * parent = Q_NULLPTR);
	vtkSmartPointer<vtkActor> GetActor();
	~TorusDialog();


private:
	Ui::TorusDialog *ui;
};

#endif // TORUSDIALOG_HPP