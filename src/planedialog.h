﻿#pragma once
#include <QDialog>
#include "tvarodialog.h"

#include <vtkSmartPointer.h>
#include <vtkPlaneSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkTriangleFilter.h>


namespace Ui {class PlaneDialog;}

class PlaneDialog : public TvaroDialog {
	Q_OBJECT

public:
	PlaneDialog(QDialog * parent = Q_NULLPTR);
	vtkSmartPointer<vtkActor> GetActor();
	~PlaneDialog();

private:
	Ui::PlaneDialog *ui;
};
